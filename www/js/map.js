var Map = {
	el: {
		2	: {handicap: false},
		3	: {handicap: true},
	},

	fieldSize: {
		width: 50,
		height: 50
	},

	field: [
		[1, 0, 2, 3, 2, 2 ,2, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2 ,2, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2 ,2, 3, 2, 3, 3, 2, 2, 2, 3, 2, 2 ,2, 3, 2, 3, 3],
		[2, 2, 2, 2, 2, 2 ,2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 3, 2, 2, 2],
		[3, 3, 2, 2, 2, 3 ,2, 2, 2, 2, 3, 3, 3, 2, 2, 2, 3 ,2, 2, 2, 2, 3, 3, 3, 2, 2, 2, 3 ,2, 2, 2, 2, 3, 3, 3, 2, 2, 2, 3 ,2, 2, 2, 2, 3],
		[2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2],
		[2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2],
		[3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3],
		[2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2],
		[3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3],
		[2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2],
		[3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3],
		[2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2],
		[3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3],
		[2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3 ,2, 2, 2, 3, 2],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 3],
		[2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ,2, 2, 2, 2, 2],
		[3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3, 3, 2, 3, 2, 3, 2 ,2, 2, 2, 3, 3],
	]
};
