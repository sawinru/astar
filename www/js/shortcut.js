var Shortcut = function(start, end) {
	// список точек, которые нужно проверить
	var open = [start];
	// список проверенных точек
	var close = [];
	var G = {},
		F = {},
		parent = {};

	G[start] = 0;
	F[start] = G[start] + h(start, end);

	var current		= null,
		neighbor	= null,
		tempG		= null;

	while (open.length) {
		current = getMinF(open);
		if (current != end) {
			removeOpen(current);
			close.unshift(current);

			neighbor = getNeighbor(current);
			for (var x in neighbor) {
				tempG  = G[current] + g(current, neighbor[x]);

				if (open.indexOf(neighbor[x]) < 0 || tempG < G[neighbor[x]]) {
					parent[neighbor[x]] = current;
					G[neighbor[x]] = tempG;
					F[neighbor[x]] = tempG + h(neighbor[x], end);
				}

				// Добавляем в список
				if (open.indexOf(neighbor[x]) < 0) {
					open.push(neighbor[x]);
				}
			}

		} else {
			break;
		}
	}


	/**
	 * стоимость передвижения из стартовой точки к точку x
	 */
	function g(current, target) {
		current	= current.split('-');
		target	= target.split('-');

		var distance = {
			x: Math.abs(current[1] - target[1]),
			y: Math.abs(current[0] - target[0])
		};

		if (distance.x < distance.y) {
			return (14 * distance.x) + (10 * (distance.y - distance.x));
		} else {
			return (14 * distance.y) + (10 * (distance.x - distance.y));
		}
	}

	/**
	 * примерная стоимость передвижения от точки current до target (Manhattan method)
	 */
	function h(current, target) {
		current	= current.split('-');
		target	= target.split('-');

		return 10 * (Math.abs(current[0] - target[0]) + Math.abs(current[1] - target[1]));
	}


	function getMinF(list) {
		var min = null;
		for (var x in list) {
			if (!min || F[list[x]] < F[min]) {
				min = list[x];
			}
		}

		return min;
	}

	function removeOpen(key) {
		var index = open.indexOf(key);
		if (index > -1) {
			open.splice(index, 1);
		}
	}

	function getNeighbor(current) {
		current	= current.split('-');
		var list = [];

		var option = [
			{line: -1, column: -1}, //top-left
			{line: -1, column: 0}, //top
			{line: -1, column: 1}, //top-right,
			{line: 0, column: -1}, //left
			{line: 0, column: 1}, //right
			{line: 1, column: -1}, //bottom-left
			{line: 1, column: 0}, //bottom
			{line: 1, column: 1} //bottom-right
		];

		var line, column = '';
		for (var i = 0; i < option.length; i++) {
			line = +current[0] + option[i].line;
			column = +current[1] + option[i].column;

			// проходим по ближайшим точкам
			if (
				Map.field[line] &&
				Map.field[line][column] &&
				close.indexOf(line + '-' + column) < 0 && // игнорируем выбранные точки
				!Map.el[Map.field[line][column]].handicap // игнорируем препятствия
			) {
				list.push(line+'-'+column);
			}
		}

		return list;
	}

	function getPath() {
		var path	= [];
		path.unshift(close[0]);

		var el = parent[close[0]];
		while (el != start) {
			path.unshift(el);
			el = parent[el];
		}
		path.push(end);

		return path;
	}

	return {
		path	: getPath(),
		g		: G[end]
	};
};