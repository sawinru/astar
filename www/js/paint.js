var Paint = function (ctx, width, height) {
	this.moveSpeed	= 0.5;

	this.storage	= {
		field	: {},
		hero	: null,
		build	: {}
	};

	/**
	 * Отображение карты
	 */
	this.addMap = function () {
		for (var i = 0; i < Map.field.length; i++) {
			for(var j = 0; j < Map.field[i].length; j++) {
				var image = new Image();
				image.src = 'img/field/' + Map.field[i][j] + '.jpg';

				this.storage.field[i+'-'+j] = {
					x		: j * Map.fieldSize.width,
					y		: i * Map.fieldSize.height,
					width	: Map.fieldSize.width,
					height	: Map.fieldSize.height,
					image	: image,
					selected: false,
					path	: false
				};
			}
		}

		this.drow();
	};

	this.addHero = function (data) {
		this.storage.hero = data;
		this.drow();
	};

	/**
	 * Движение героя
	 */
	this.heroMove = function (route) {
		var current	= 0,
			x = 0,
			y = 0;


		function move() {
			if (route.path.length) {
				setTimeout(function () {
					current = route.path[0].split('-');
					this.storage.hero.position.line = current[0];
					this.storage.hero.position.column = current[1];
					this.storage.field[route.path[0]].route = false;
					this.storage.field[route.path[0]].selected = false;

					this.drow();
					route.path.splice(0, 1);
					move.call(this);
				}.bind(this), 250 * this.moveSpeed);
			}
		}

		move.call(this);
	};

	this.selectField = function (line, column) {
		var field = this.storage.field[line+'-'+column];
		if (field) {
			field.selected = true;
			this.drow();

			return {line: line, column: column};
		}
		return false;
	};

	/**
	 * Отменяем все селекты
	 */
	this.unSelectField = function () {
		for (x in this.storage.field) {
			this.storage.field[x].selected	= false;
			this.storage.field[x].route		= false;
		}

		this.drow();
	};

	this.addPath = function (route) {
		for (var x in route.path) {
			var field = this.storage.field[route.path[x]];
			if (field) {
				field.route = true;
			}
		}
		this.drow();
	};

	/**
	 * Отрисовываем все объекты
	 */
	this.drow = function () {
		// чистим холст
		this.clear();
		var x 	= 0,
			el	= null;

		for (x in this.storage.field) {
			el = this.storage.field[x];

			context.drawImage(el.image, el.x, el.y, el.width, el.height);
			if (el.selected) {
				ctx.strokeRect(el.x, el.y, el.width, el.height);
			}
			if (el.route) {
				ctx.beginPath();
				ctx.arc(el.x + el.width/2, el.y + el.height/2, el.width/5, 0, 2*Math.PI, false);
				ctx.fillStyle = 'green';
				ctx.fill();
			}
		}

		if (this.storage.hero) {
			context.drawImage(
				this.storage.hero.image,
				this.storage.hero.position.column * Map.fieldSize.width,
				this.storage.hero.position.line * Map.fieldSize.height,
				48,
				48
			);
		}
	};

	/**
	 * Чистит холст
	 */
	this.clear = function () {
		ctx.clearRect(0, 0, width, height);
	};
};