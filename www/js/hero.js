var Hero = function (params) {
	this.race		= params.race;
	this.position	= {line: 0, column: 0};
	this.image		= new Image();

	switch (this.race) {
		default: {
			this.image.src = 'img/heroes/viking.gif';
		}
	}
};